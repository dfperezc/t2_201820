package model.data_structures;

import java.util.Collection;
import java.util.Iterator;

public class DoublyLinkedList<T> implements IDoublyLinkedList<T> {

	protected int cantidadElementos;

	/**
	 * Primer nodo de la lista.
	 */
	protected Node<T> primerNodo;

	public DoublyLinkedList(T elemento)
	{
		primerNodo = new Node<T>(elemento);
	}
	/**
	 * Indica el tama�o de la lista.
	 * @return La cantidad de nodos de la lista.
	 */
	public int getSize() 
	{
		return cantidadElementos;
	}

	/**
	 * Reemplaza el elemento de la posici�n por el elemento que llega por par�metro.
	 * @param index La posici�n en la que se desea reemplazar el elemento.
	 * @param element El nuevo elemento que se quiere poner en esa posici�n
	 * @return el m�todo que se ha retirado de esa posici�n. //n9 retorna null si la lista estaba vac�a
	 * @throws IndexOutOfBoundsException si la posici�n es < 0 o la posici�n es >= size()
	 */
	public T set(int index, T element) throws IndexOutOfBoundsException 
	{
		if( index < 0 || getSize() <= index)
		{
			throw new IndexOutOfBoundsException();
		}
		T resp = null;
		Node<T> nuevo = new Node<T>( element );

		if(index == 0)
		{

			nuevo.cambiarSiguiente( primerNodo.darSiguiente() );
			resp = primerNodo.darElemento();
			primerNodo.cambiarSiguiente(null);
			primerNodo = nuevo;

			return resp;

		}

		else
		{
			Node<T> nuevoII = primerNodo;
			Node<T> nuevoIII = nuevoII.darSiguiente();
			int indActual = 0;

			while( indActual < index-1 )
			{
				indActual ++;
				nuevoII = nuevoII.darSiguiente();
				nuevoIII = nuevoII.darSiguiente();
			}

			nuevo.cambiarSiguiente( nuevoIII.darSiguiente( ) );
			nuevoII.cambiarSiguiente(nuevo);
			resp = nuevoIII.darElemento();
			nuevoIII.cambiarSiguiente(null);

			return resp;
		}
	}

	
	public boolean add(E e) 
	{
		// TODO Completar seg�n la documentaci�n
		if(e == null)
		{
			throw new NullPointerException("Se recibe un elemento nulo");
		}


		Node<T> nodoDeVerificacion = (Node<T>)primerNodo;
		Node<T> nodoAserAgregado = new Node<T>(e);
		int i = 0;
		while( i <cantidadElementos)
		

		nodoDeVerificacion.cambiarSiguiente(nodoAserAgregado);
		Node<T> postActual = (Node<T>) nodoDeVerificacion.darSiguiente();
		postActual.cambiarAnterior(nodoDeVerificacion);




		return true;
	}



	/**
	 * Devuelve un iterador sobre la lista
	 * El iterador empieza en el primer elemento
	 * @return un nuevo iterador sobre la lista
	 */
	public Iterator<T> iterator() 
	{
		return new IteradorListaDoble<T>(primerNodo);
	}
	

	protected class IteradorListaDoble<T> implements Iterator<T>  
	{
		protected int posicionarray;
		protected Node<T> siguienteNodo;
		protected Node<T> actualNodo;
		protected Node<T> anteriorNodo;

		public IteradorListaDoble( Node<T> pPrimerNodo) 
		{
			posicionarray = 0; 
			actualNodo = pPrimerNodo;
			siguienteNodo = actualNodo.darSiguiente();
			anteriorNodo= null;
        
		}

		public boolean hasNext() 
		{
			return siguienteNodo != null;
		}

		
	
		

		public Node<T> getNext()
		{
			anteriorNodo = actualNodo;
			actualNodo = siguienteNodo;
			return actualNodo;
		}

		@Override
		public T next() 
		{
			return siguienteNodo.darElemento();
		}
	}
	
	public T getObject()
	{
		return primerNodo.darElemento();
	}
	
	public Node<T> getFirst()
	{
		return primerNodo;
	}



	


}
