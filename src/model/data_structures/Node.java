package model.data_structures;

public class Node<E> {
	
	/**
	 * Nodo anterior.
	 */
	private Node<E>anterior;
	
	/**
	 * Siguiente nodo.
	 */
	protected Node<E> siguiente;
	
	/**
	 * Elemento almacenado en el nodo.
	 */
	protected E elemento;
	
	/**
	 * M�todo constructor del nodo doblemente encadenado
	 * @param elemento elemento que se almacenar� en el nodo.
	 */
	public Node(E elemento) 
	{
		this.elemento = elemento;
		siguiente = null;
		anterior = null;
	}
	
	/**
	 * M�todo que retorna el nodo anterior.
	 * @return Nodo anterior.
	 */
	public Node<E> darAnterior()
	{
		return anterior;
	}
	
	/**
	 * M�todo que cambia el nodo anterior por el que llega como par�metro.
	 * @param anterior Nuevo nodo anterior.
	 */
	public void cambiarAnterior(Node<E> anterior)
	{
		this.anterior = anterior;
	}

	/**
	 * M�todo que retorna el nodo anterior.
	 * @return Nodo anterior.
	 */
	public Node<E> darSiguiente()
	{
		return anterior;
	}
	
	/**
	 * M�todo que cambia el nodo anterior por el que llega como par�metro.
	 * @param anterior Nuevo nodo anterior.
	 */
	public void cambiarSiguiente(Node<E> anterior)
	{
		this.anterior = anterior;
	}
	
	/**
	 * M�todo que retorna el elemento almacenado en el nodo.
	 * @return El elemento almacenado en el nodo.
	 */
	public E darElemento()
	{
		//TODO Completar de acuerdo a la documentaci�n.
		return elemento;
	}
}
