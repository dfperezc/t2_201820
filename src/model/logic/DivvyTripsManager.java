package model.logic;

import api.IDivvyTripsManager;
import model.vo.VOByke;
import model.vo.VOTrip;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.DoublyLinkedList;
import model.data_structures.Node;

public class DivvyTripsManager <T> implements IDivvyTripsManager 
{

	DoublyLinkedList listaDeViajesLeidaDelArchivoOriginal;

	public void loadStations (String stationsFile) {
		// TODO Auto-generated method stub

	}


	public void loadTrips (String tripsFile) {
		// TODO Auto-generated method stub

	}

	@Override
	public IDoublyLinkedList <T> getTripsOfGender (String gender) 
	{
		DoublyLinkedList<T> resp = new DoublyLinkedList<T>((T)listaDeViajesLeidaDelArchivoOriginal.getFirst().darElemento());
		Node<T> nodoActual = listaDeViajesLeidaDelArchivoOriginal.getFirst();

		while(nodoActual !=null)
		{
			VOTrip viajeQueEstaSiendoleido = (VOTrip)listaDeViajesLeidaDelArchivoOriginal.getFirst().darElemento();
			if(viajeQueEstaSiendoleido.getGender().equals(gender))
			{
				resp.add(viajeQueEstaSiendoleido);
			}
			nodoActual = nodoActual.darSiguiente();
		}
		return resp;
	}

	//se supone que la lista (listaDeViajesLeidaDelArchivoOriginal) almacena VOTrips
	@Override
	public IDoublyLinkedList <VOTrip> getTripsToStation (int stationID) 
	{
		DoublyLinkedList<VOTrip> resp = new DoublyLinkedList<VOTrip>();
		Node nodoActual = listaDeViajesLeidaDelArchivoOriginal.getFirst;

		while(nodoActual !=null)
		{
			VOTrip viajeQueEstaSiendoleido = listaDeViajesLeidaDelArchivoOriginal.getFirst().getObject();
			if(viajeQueEstaSiendoleido.getToStation().equals(String.valueOf(stationID)))
			{
				resp.add(viajeQueEstaSiendoleido);
			}
			nodoActual = nodoActual.getNext();
		}
		return resp;
	}


	@Override
	public void loadServices(String stationsFile) {
		// TODO Auto-generated method stub

	}


}
