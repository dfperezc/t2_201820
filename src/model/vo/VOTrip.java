package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	private int id;
	private double tripSeconds;
	private String fromStation;
	private String toStation;
	private String gender;
	
	public VOTrip(int pid, double pgettrip, String fromS, String toS, String pG)
	{
		id = pid;
		tripSeconds = pgettrip;
		toStation = toS;
		fromStation = fromS;
		gender = pG;
	}
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return id;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return tripSeconds;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return fromStation;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return toStation;
	}
	
	public String getGender()
	{
		return gender;
	}
}
